package org.example;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException,    ClassNotFoundException, InterruptedException{

        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "VGSales");

        //first argument is job itself
        //second argument is location of the input dataset
        FileInputFormat.addInputPath(job, new Path(args[0]));

        //first argument is the job itself
        //second argument is the location of the output path
        FileOutputFormat.setOutputPath(job, new Path(args[1]));


        job.setJarByClass(App.class);

        job.setMapperClass(VgMapper.class);

        job.setReducerClass(VGReducer.class);

        job.setOutputKeyClass(CompositeGroupKey.class);

        job.setOutputValueClass(FloatWritable.class);


        //setting the second argument as a path in a path variable
        Path outputPath = new Path(args[1]);

        //deleting the output path automatically from hdfs so that we don't have delete it explicitly
        outputPath.getFileSystem(conf).delete(outputPath);


        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
