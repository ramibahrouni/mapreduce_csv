package org.example;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class CompositeGroupKey implements WritableComparable<CompositeGroupKey> {

    String year;
    String publisher;

    public CompositeGroupKey(String year, String publisher) {
        this.year = year;
        this.publisher = publisher;
    }

    public CompositeGroupKey() {
        this.year = "";
        this.publisher = "";
    }

    @Override
    public int compareTo(CompositeGroupKey o) {
        if (o == null)
            return 0;
        int intyear = year.compareTo(o.year);
        return intyear == 0 ? publisher.compareTo(o.publisher) : intyear;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        WritableUtils.writeString(dataOutput, publisher);
        WritableUtils.writeString(dataOutput, year);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.publisher = WritableUtils.readString(dataInput);
        this.year = WritableUtils.readString(dataInput);

    }

    @Override
    public String toString() {
        return year.toString() + ":" + publisher.toString();
    }
}
