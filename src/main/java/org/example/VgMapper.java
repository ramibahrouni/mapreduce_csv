package org.example;


import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class VgMapper extends Mapper<LongWritable, Text, CompositeGroupKey, FloatWritable> {
//    CompositeGroupKey cgk = new CompositeGroupKey();
//
//    Text year = new Text();
//    Text publisher = new Text();
    FloatWritable sales;

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            try {
                if (key.get() == 0 && value.toString().contains("header") /*Some condition satisfying it is header*/)
                    return;
                else {
                    // For rest of data it goes here
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        String line = value.toString();
        String[] keyvalue = line.split(",");
        sales.set(Float.parseFloat(keyvalue[10].trim()));
        CompositeGroupKey cgk = new CompositeGroupKey(keyvalue[3], keyvalue[5]);
        context.write(cgk, sales);
    }
}
