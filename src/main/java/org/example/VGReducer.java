package org.example;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

public class VGReducer extends Reducer<CompositeGroupKey, FloatWritable, CompositeGroupKey, FloatWritable> {
    public void reduce(CompositeGroupKey key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {

        //int numberofelements = 0;

        float cnt = 0;

        for (FloatWritable value : values) {
            cnt += value.get();
        }

        context.write(key, new FloatWritable(cnt));

        //context.write(key, new FloatWritable(cnt));

    }
}
